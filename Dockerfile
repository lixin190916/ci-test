# 镜像文件
FROM node:10-alpine

# 镜像中项目路径
WORKDIR /server
# 拷贝当前目录代码到镜像
COPY / /server

RUN npm config set registry https://registry.npm.taobao.org && npm install

CMD ["npm", "start"]